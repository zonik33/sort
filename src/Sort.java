import java.util.Arrays;


/**
 * @author Костюков К.П 17ит18
 */

class Main {

    public static void main(String[] args) {
        int[] arr = {0, 2, 9, 1, 9, 4, 4, -1};
        System.out.println(Arrays.toString(Sort.sort(arr)));
    }

}

public class Sort {

    /**
     * В методе производится сортировка массива по частоте
     *
     * @param arr - массив
     * @return - возврат отсортированного массива
     */
        public static int[] sort(int[] arr) {

            int[][] twoDimensionalArray = frequencySorting(arr);

            int counter = 0;

            for (int i = 0; i < twoDimensionalArray[1].length; i++) {
                int numberOccurrences = 0, value = 0, index = 0;

                for (int j = 0; j < twoDimensionalArray[1].length; j++) {
                    if (numberOccurrences < twoDimensionalArray[1][j] && twoDimensionalArray[1][j] != 0) {
                        numberOccurrences = twoDimensionalArray[1][j];
                        index = j;
                        value = twoDimensionalArray[0][j];
                    }
                }
                for (int g = 0; g < numberOccurrences; g++) {
                    arr[counter] = value;
                    counter++;
                }
                twoDimensionalArray[1][index] = 0;
            }
            return arr;
        }

        /**
         *
         * @param arr - массив
         * @return - возврат отсортированного массима с количеством чисел
         */
        private static int[][] frequencySorting(int[] arr) {
            int[][] twoDimensionalArray = new int[2][arr.length];

            int counter = 0;

            for (int value : arr) {
                if (items(twoDimensionalArray, value)) {
                    twoDimensionalArray[0][counter] = value;
                    for (int i : arr) {
                        if (twoDimensionalArray[0][counter] == i) {
                            twoDimensionalArray[1][counter]++;
                        }
                    }
                    counter++;
                }
            }
            return twoDimensionalArray;
        }

        /**
         * В методе проверяется, есть ли в массиве похожее число
         *
         * @param arr   - массив
         * @return - возврат результата проверки на повтор чисел
         */

        private static boolean items(int[][] arr, int value) {
            boolean permission = true;

            for (int j = 0; j < arr[0].length; j++) {
                if (arr[0][j] == value && arr[1][j] != 0) {
                    permission = false;
                    break;
                }
            }
            return permission;
        }
    }

